import threading
import serial
import requests
import time
import serial
import serial.tools.list_ports as list_ports
from flask import Flask, request, jsonify


app = Flask(__name__)

URL_RESIDENCIA = 'http://localhost:5000'
PID_MICROBIT = 516
VID_MICROBIT = 3368
TIMEOUT = 0.1
SERIAL_PORT = None


# Función para encontrar el puerto al que microbit está conectado
def find_microbit(pid, vid, baud):
    serial_port = serial.Serial(timeout=TIMEOUT)
    serial_port.baudrate = baud
    ports = list(list_ports.comports())
    print('Scanning ports...')
    for p in ports:
        try:
            if (p.pid == pid) and (p.vid == vid):
                print(f'Found device at {p.device} [pid : {p.pid} | vid : {p.vid}]')
                serial_port.port = str(p.device)
                global SERIAL_PORT
                SERIAL_PORT = serial_port
        except AttributeError:
            continue


# Función para procesar mensajes de micro:bit
def read_from_microbit():
    while True:
        time.sleep(0.1)
        received_message = SERIAL_PORT.readline().decode('utf-8').strip()
        if received_message:
            print(">", received_message)
            if not received_message.startswith("ACK"):
                send_to_residencia(received_message)


# Endpoint para enviar mensaje al micro:bit y residencia
@app.route('/send', methods=['POST'])
def send_to_microbit():
    message = request.json.get('message').strip()
    if message:
        print(">", message)
        SERIAL_PORT.write(message.encode())
        send_to_residencia(message)
        return jsonify({'status': 'success', 'message': 'Mensaje enviado con éxito'})
    return jsonify({'status': 'error', 'message': 'No se pudo enviar el mensaje'})


# Función para enviar un mensaje a la residencia
def send_to_residencia(message):
    print("Enviando mensaje a residencia...")
    requests.post(f"{URL_RESIDENCIA}/store_message", json={"message": message})


def main():
    # Encontrar y abrir puerto al que está conectado el micro:bit
    find_microbit(PID_MICROBIT, VID_MICROBIT, 115200)
    if not SERIAL_PORT:
        print("Couldn't find microbit")
        return
    print('Opening and monitoring microbit port...')
    SERIAL_PORT.open()

    # Crear hilos para procesar mensajes del micro:bit
    microbit_thread = threading.Thread(target=read_from_microbit)
    microbit_thread.start()

    # Ejecutar la app
    app.run(debug=False, port=6000)


if __name__ == "__main__":
    main()