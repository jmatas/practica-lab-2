# Laboratorio II

## Práctica: _base_station.py_ y _residencia_py_

En esta práctica hemos desarrollado los archivos base_station.py y residencia.py, los cuales procesarán una serie de solicitudes HTTP para permitir el almacenamiento de mensajes en la base de datos residencia.db.

Puesto que hemos de abrir varias terminales espeficaremos la terminal que estamos usando en caso de ser pertinente mediante el texto:

```bash
# nombre de la terminal
```

En caso de no aparecer se entiende que el comando se puede ejecutar en una terminal cualquiera.

Hecho el código, adjunto en el repositorio, el proceso de ejecución es el siguiente:

### 0. myenv

En caso de haber creado una variable de entorno podemos activarla desde la terminal con el comando:

```bash
source myenv/bin/activate
```

Esto nos permitirá utilizar una versión aislada de Python, en lugar de la versión que tengamos en nuestra máquina. En caso de utilizar una variable de entorno debemos ejecutar este comando en todas las terminales que abramos durante todo el ejercicio.

Sabremos que se ha activado correctamente porque veremos **(myenv)** (en caso de haberla llamado así) al lado del prompt de la terminal.

### 1. residencia.py

Puesto que la residencia va a procesar las solicitudes de base_station ésta es la primera que hemos de iniciar. En caso de iniciar antes base_station podría recibirse un mensaje pero no enviarse todavía a la residencia.

Para ello ejecutamos desde la terminal:

```bash
# residencia
python3 residencia.py
```

Si todo va bien debería aparecer el siguiente texto:

```bash
# residencia
python3 residencia.py
 * Serving Flask app 'residencia'
 * Debug mode: off
WARNING: This is a development server. Do not use it in a production deployment. Use a production WSGI server instead.
 * Running on http://127.0.0.1:5000

```

### 2. base_station.py

Ahora sí podemos iniciar base_station. Al tratarse de una app Flask al igual que residencia.py la iniciamos del mismo modo.

```bash
# base_station
python3 base_station.py
```

Si todo va bien debería aparecer el siguiente texto:

```bash
# base_station
python3 base_station.py
Scanning ports...
Found device at /dev/ttyACM0 [pid : 516 | vid : 3368]
Opening and monitoring microbit port...
 * Serving Flask app 'base_station'
 * Debug mode: off
WARNING: This is a development server. Do not use it in a production deployment. Use a production WSGI server instead.
 * Running on http://127.0.0.1:6000
```

### 3. Solicitudes HTTP

Ahora que tenemos iniciado base_station podemos recibir y enviar mensajes. Como vemos estamos corriendo la aplicación en el puerto 6000, de modo que podemos realizar llamadas HTTP para añadir mensajes a la residencia y almacenarlos en la DB.

Hay muchas maneras de hacer esto, en este caso utilizaremos el comando **curl**.

**Nota:** Debido a la manera en que estamos gestionando las solicitudes HTTP necesitamos que el contenido recibido llegue en formato JSON. Esto lo especificamos añadiendo en la cabecera **_Content-Type: aplication/json_**.

```bash
 curl -X POST -H "Content-Type: application/json" -d '{"message":"Mensaje"}' http://localhost:6000/send
```

Esta solicitud es procesada por base_station.py, concretamente en:

```python
# Endpoint para enviar mensaje al micro:bit y residencia
@app.route('/send', methods=['POST'])
def send_to_microbit():
    message = request.json.get('message').strip()
    if message:
        print(">", message)
        SERIAL_PORT.write(message.encode())
        send_to_residencia(message)
        return jsonify({'status': 'success', 'message': 'Mensaje enviado con éxito'})
    return jsonify({'status': 'error', 'message': 'No se pudo enviar el mensaje'})

# Función para enviar un mensaje a la residencia
def send_to_residencia(message):
    print("Enviando mensaje a residencia...")
    requests.post(f"{URL_RESIDENCIA}/store_message", json={"message": message})
```

Como vemos, se realiza otra solicitud HTTP a la residencia para almacenar el mensaje. Esta solicitud es procesada por _residencia.py_ en:

```python
# Endpoint para almacenar mensaje en db
@app.route('/store_message', methods=['POST'])
def save_message():
    content = request.json.get('message').strip()
    if content:
        db = get_db()
        cursor = db.cursor()
        cursor.execute('INSERT INTO messages(content) VALUES (?)', (content,))
        db.commit()
        db.close()
        return jsonify({'status': 'success', 'message': 'Mensaje recibido'})
    else:
        return jsonify({'status': 'error', 'message': 'No se recibió ningún mensaje'})
```

En cada terminal debe aparecer lo siguiente:

```bash
# base_station
> Mensaje
Enviando mensaje a residencia...
127.0.0.1 - - [14/Dec/2023 16:23:30] "POST /send HTTP/1.1" 200 -
> ACK: Mensaje
```

```bash
# residencia
127.0.0.1 - - [14/Dec/2023 16:23:30] "POST /store_message HTTP/1.1" 200 -
```

La línea `127.0.0.1 - - [14/Dec/2023 16:23:30] "POST /send HTTP/1.1" 200 -` es el log de la solitud HTTP que ha procesado el endpoint.

El mensaje "ACK" procede del micro:bit, para confirmar que el mensaje ha sido recibido por éste correctamente. Podríamos haber añadido alguna lógica para que se reenvíe el mensaje en caso de no recibir el ACK.

La otra solicitud HTTP que nos interesa es la petición para obtener los mensajes existentes. En el código esta solicitud es procesada por _residencia.py_:

```python
# Endpoint para obtener todos los mensajes
@app.route('/messages', methods=['GET'])
def get_messages():
    db = get_db()
    cursor = db.cursor()
    cursor.execute('SELECT * FROM messages')
    messages = cursor.fetchall()
    db.close()

    messages = [{'id': message[0], 'content': message[1]} for message in messages]
    return jsonify(messages)
```

Realizamos la llamada correspondiente y obtenemos lo siguiente:

```bash
curl http://localhost:5000/messages

[{"content":"Mensaje","id":1},{"content":"Pacman","id":2},{"content":"Skull","id":3},{"content":"Pato","id":4}]
```

En este caso no ha sido necesario especificar el tipo de solicitud, porque por defecto se realiza un GET, ni incluir contenido en el cuerpo.

### 4. Micro_bit

Además de enviar directamente solicitudes HTTP también podemos utilizar los botones del Micro:bit para enviar mensajes. Tenemos configuradores tres posibles mensajes:

- Pacman (A)
- Skull (B)
- Pato (A+B)

Cada opción enviará un mensaje por pantalla y mostrará por el micro:bit su respectivo icono.

Mostramos en este orden las 3 combinaciones, de modo que se envía primero "Pacman", luego "Skull" y finalmente "Pato". En este ejercicio es base_station el encargado de leer la entrada del micro:bit, de modo que debería aparecer en la terminal en que hemos ejecutado base_station:

```bash
# base_station
> Pacman
Enviando mensaje a residencia...
> Skull
Enviando mensaje a residencia...
> Pato
Enviando mensaje a residencia...
```

Estos mensajes son enviados de _base_station.py_ a _residencia.py_ en el siguiente trozo de código:

```python
# Función para procesar mensajes de micro:bit
def read_from_microbit():
    while True:
        time.sleep(0.1)
        received_message = SERIAL_PORT.readline().decode('utf-8').strip()
        if received_message:
            print(">", received_message)
            if not received_message.startswith("ACK"):
                send_to_residencia(received_message)
```

### 5. residencia.db

Todos estos mensajes se están almacenando en residencia.db. Para comprobarlo podemos usar sqlite3 desde la terminal:

```bash
sqlite3 residencia.db
SQLite version 3.44.0 2023-11-01 11:23:50
Enter ".help" for usage hints.
sqlite> SELECT * FROM messages;
1|Mensaje
2|Pacman
3|Skull
4|Pato
```
