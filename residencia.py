from flask import Flask, request, jsonify
import sqlite3

app = Flask(__name__)

DATABASE_NAME = "residencia.db"


# Obtener conexión a db
def get_db():
    conn = sqlite3.connect(DATABASE_NAME)
    return conn;


# Función para crear la base de datos
def create_tables():
    tables = [
        """
        CREATE TABLE IF NOT EXISTS messages(
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            content TEXT NOT NULL
            )
        """
    ]
    db = get_db()
    cursor = db.cursor()
    for table in tables:
        cursor.execute(table)
    db.commit()
    db.close()


# Endpoint para almacenar mensaje en db
@app.route('/store_message', methods=['POST'])
def save_message():
    content = request.json.get('message').strip()
    if content:
        db = get_db()
        cursor = db.cursor()
        cursor.execute('INSERT INTO messages(content) VALUES (?)', (content,))
        db.commit()
        db.close()
        return jsonify({'status': 'success', 'message': 'Mensaje recibido'})
    else:
        return jsonify({'status': 'error', 'message': 'No se recibió ningún mensaje'})


# Endpoint para obtener todos los mensajes
@app.route('/messages', methods=['GET'])
def get_messages():
    db = get_db()
    cursor = db.cursor()
    cursor.execute('SELECT * FROM messages')
    messages = cursor.fetchall()
    db.close()

    messages = [{'id': message[0], 'content': message[1]} for message in messages]
    return jsonify(messages)


def main():
    create_tables()
    app.run(debug=False, port=5000)


if __name__ == "__main__":
    main()